import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts;

    //default constructor
    Phonebook() {
        this.contacts = new ArrayList<>();
    };

    //parameterized constructor
    Phonebook(Contact contacts) {
        this.contacts = new ArrayList<>();
    }

    //setter
    public void setContacts(Contact contact) {
        contacts.add(contact);
    }

    //getter
    public ArrayList<Contact> getContacts(){
        return contacts;
    }

    //get the number of items



}
